package for_project;
import java.util.LinkedList;
import java.util.List;
//Abby Jeffers
//Cerina Bhavnani
//Emily Keller
public class SeparateChainingHashTable<E>
{
	public SeparateChainingHashTable( )
	{
		this( DEFAULT_TABLE_SIZE );
	}

	//RUNTIME
	//since we are walking down all of the indexes of our tables to add
	//add the linked lists, our running time is O(n) 
	public SeparateChainingHashTable( int initialSize )
	{
		nameSize = 0;
		numberSize = 0;
		nameTable = new LinkedList[ nextPrime( initialSize ) ];
		numberTable = new LinkedList[ nextPrime( initialSize ) ];
		for( int i = 0; i < nameTable.length; i++ )
			nameTable[ i ] = new LinkedList<>( );
		for( int i = 0; i < numberTable.length; i++ )
			numberTable[ i ] = new LinkedList<>( );
	}

	//RUNTIME
	//we have to walk through each character of our string
	//so the runtime is O(n), but it is very small 
	//because our strings should be very small. On average, 
	//we have Θ(1)
	public String formatNumber( String number )
	{
		StringBuilder sb = new StringBuilder( number );
		for( int i = 0; i < sb.length(); i++ )
		{
			if( !Character.isDigit( sb.charAt( i ) ) )
				sb.deleteCharAt( i );
		}
		return sb.toString();
	}

	//RUNTIME
	//we have to walk through each character of our string
	//so the runtime is O(n), but it is very small 
	//because our strings should be very small. On average, 
	//we have Θ(1)
	public boolean isNumber( String nameORnumber )
	{
		StringBuilder sb = new StringBuilder( nameORnumber );
		for( int i = 0; i < sb.length(); i++ )
		{
			if( Character.isDigit( sb.charAt( i ) ) )
				return true;
		}
		return false;
	}

	//RUNTIME
	//In some places we are running a few comparisons, and building links and variables,
	//which are constant time. Then, we call the hash function, which is Θ(1)avg.
	//Then we walk though a linked list at the index we hashed to. This is O(n) worst case, but
	//is Θ(1) on average. 
	//Then we run some more constant time comparisons. 
	//Worst case, we have to rehash, which is O(n). However, on average our running time
	//is Θ(1)
	public boolean insert(String name, String number)
	{
		if(isNumber(name) || !isNumber(number)) {
			return false;
		}
		number = formatNumber(number);
		List<Contact> whichNameList = nameTable[ hash( name, nameTable.length ) ];
		List<Contact> whichNumberList = numberTable[ hash( number, numberTable.length) ];
		boolean addedName = false;
		boolean addedNumber = false;
		Contact newContact = new Contact( name, number );
		boolean duplicate = false;
		for( Contact currentContact: whichNameList )
		{
			if( currentContact.name.equals( newContact.name ) || currentContact.number.equals( newContact.number ) )
				duplicate = true;
		}
		for( Contact currentContact: whichNumberList )
		{
			if( currentContact.name.equals( newContact.name ) || currentContact.number.equals( newContact.number ) )
				duplicate = true;
		}if( !duplicate )
		{
			if( whichNameList.add( newContact ) )
				addedName = true;
		}
		if( !duplicate )
		{
			if( whichNumberList.add( newContact ) )
				addedNumber = true;
		}
		if( ++nameSize > nameTable.length || ++numberSize > numberTable.length )
			rehash();
		return addedName && addedNumber;
	}
	//calls the isNumber function and the deleteWithName or deleteWithNumber function
	//each of these are worst case O(n), but Θ(1) on average. 
	public boolean delete( String nameORnumber )
	{
		if( !isNumber( nameORnumber ) )
			return deleteWithName( nameORnumber );
		if( isNumber( nameORnumber ) )
			return deleteWithNumber( formatNumber( nameORnumber ) );
		return false;
	}

	//We are calling hash and walking through a linked list. Each of these are on 
	//average Θ(1), so our average runtime is Θ(1).
	public boolean deleteWithName( String name )
	{
		List<Contact> whichList = nameTable[ hash( name, nameTable.length) ];
		String numberToBeDeleted;
		for( Contact currentContact: whichList )
		{
			if( currentContact.name.equals( name ) )
			{
				numberToBeDeleted = currentContact.number;
				whichList.remove( currentContact );
				nameSize--;
				deleteWithNumber( numberToBeDeleted );
				return true;
			}
		}

		return false;
	}
	//We are calling hash and walking through a linked list. Each of these are on 
	//average Θ(1), so our average runtime is Θ(1).
	public boolean deleteWithNumber( String number )
	{
		List<Contact> whichList = numberTable[ hash( number, numberTable.length ) ];
		String nameToBeDeleted;
		for( Contact currentContact: whichList )
		{
			if( currentContact.number.equals( number ) )
			{
				nameToBeDeleted = currentContact.name;
				whichList.remove( currentContact );
				numberSize--;
				deleteWithName( nameToBeDeleted );
				return true;
			}
		}

		return false;
	}
	//calls isNumber and findName or findNumber, which are on
	//average Θ(1).
	public String find(String nameORnumber)
	{
		if( !isNumber( nameORnumber ) )
			return findName( nameORnumber );
		if( isNumber( nameORnumber ) )
			return findNumber( formatNumber( nameORnumber ) );
		return null;
	}
	//We are calling hash and walking through a linked list. Each of these on 
	//average are Θ(1), so our average runtime is Θ(1).
	public String findName( String name )
	{
		List<Contact> whichList = nameTable[ hash( name, nameTable.length ) ];
		for( Contact currentContact: whichList )
		{
			if( currentContact.name.equals( name ) )
				return currentContact.number;
		}
		return null;
	}

	//We are calling hash and walking through a linked list. Each of these on 
	//average are Θ(1), so our average runtime is Θ(1).
	public String findNumber( String number )
	{
		List<Contact> whichList = numberTable[ hash( number, numberTable.length) ];
		for( Contact currentContact: whichList )
		{
			if( currentContact.number.equals( number ) )
				return currentContact.name;
		}
		return null;
	}

	public int size()
	{
		return nameSize;	
	}
    //We walk through our array, and at each index we walk through each LinkedList
    //stored at that index. For n contacts, we have running time of O(n). 
	public void printAllContacts()
	{
		for( List<Contact> currentList : nameTable )
			for( Contact item : currentList )
				System.out.println( "Name: " + item.name + "; Number: " +item.number );
	}
	//we are walking though our string, doing some math, and assigning values
	//The walking through the string should take O(n), but is really fast because 
	//the strings are really small. On average, we see Θ(1).
	public static int hash( String key, int tableSize )
	{
		int hashVal = 0;

		for( int i = 0; i < key.length( ); i++ )
			hashVal = 37 * hashVal + key.charAt( i );

		hashVal %= tableSize;
		if( hashVal < 0 )
			hashVal += tableSize;

		return hashVal;
	}

	//Because we are walking through two arrays of length n, we have O(n).
	private void rehash()
	{
		List<Contact>[]  oldName = nameTable;
		List<Contact>[] oldNumber = numberTable;

		// Create new double-sized, empty table
		nameTable = new LinkedList[ nextPrime( 2 * oldName.length ) ];
		numberTable = new LinkedList[ nextPrime( 2 * oldNumber.length ) ];
		for( int i = 0; i < nameTable.length; i++ )
			nameTable[ i ] = new LinkedList<>();
		for( int i = 0; i < numberTable.length; i++ )
			numberTable[ i ] = new LinkedList<>();

		// Copy table over
		nameSize = 0;
		numberSize = 0;
		for( List<Contact> currentList : oldName )
			for( Contact item : currentList )
				insert( item.name, item.number );
	}

	//we have a few assignments. This is Θ(1)
	private static class Contact
	{
		Contact( String theName, String theNumber )
		{
			name = theName;
			number = theNumber;
		}
		private String name;
		private String number;
	}

	//we are performing a few constant time math operations. This is Θ(1)
	private static int nextPrime( int n )
	{
		if( n % 2 == 0 )
			n++;

		for( ; !isPrime( n ); n += 2 )
			;

		return n;
	}
	//we are performing a few constant time math operations. This is Θ(1)
	private static boolean isPrime( int n )
	{
		if( n == 2 || n == 3 )
			return true;

		if( n == 1 || n % 2 == 0 )
			return false;

		for( int i = 3; i * i <= n; i += 2 )
			if( n % i == 0 )
				return false;

		return true;
	}

	private static final int DEFAULT_TABLE_SIZE = 101;
	private List<Contact>[] nameTable;
	private List<Contact>[] numberTable;
	private int nameSize;
	private int numberSize;
}
