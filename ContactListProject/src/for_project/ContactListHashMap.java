package for_project;
import java.util.HashMap;
//Abby Jeffers
//Cerina Bhavnani
//Emily Keller
public class ContactListHashMap 
{
	public ContactListHashMap()
	{
		nameTable = new HashMap<String, String>();
		numberTable = new HashMap<String, String>();
	}
    //The Running Time of formatNumber is O(n)
    //Because we walk through each character in the string
    //to detemine if it is a digit or not.
    //however, on average we have small Strings,
    //which run in Θ(1) time.
	public String formatNumber( String number )
	{
		number = number.replaceAll("\\s+","");
		StringBuilder sb = new StringBuilder( number );
		for( int i = 0; i < sb.length(); i++ )
		{
			if( !Character.isDigit( sb.charAt( i ) ) ) 
				sb.deleteCharAt( i );
			
		}
		return sb.toString();
	}
    //The Running Time of isNumber is O(n)
    //Because again we walk through each character in the string
    //to detemine if it is a digit or not.
    //however, on average we have small Strings,
    //which run in Θ(1) time.
	public boolean isNumber( String nameORnumber )
	{
		StringBuilder sb = new StringBuilder( nameORnumber );
		for( int i = 0; i < sb.length(); i++ )
		{
			if( Character.isDigit( sb.charAt( i ) ) )
				return true;
		}
		return false;
	}
    //The Running Time of insert is θ(1) on average
    //Because again we walk through things but on
    //average they are short hence not taking a lot
    //of time character in the string.
	public boolean insert( String name, String number ) // no duplicates
	{
		number = formatNumber( number );
		if( nameTable.putIfAbsent( name, number ) == null 
				&& numberTable.putIfAbsent( number,  name ) == null )
			return true;
		return false;
	}
    //The Running Time of find is θ(1) as nameORnumber
    //is the key that is used in the get method. This
    //has a constant running time.
	public String find( String nameORnumber )
	{
		if( !isNumber( nameORnumber ) )
			return findWithName( nameORnumber );
		if( isNumber( nameORnumber ) )
			return findWithNumber( formatNumber( nameORnumber ) );
		return null;
	}
    //Running Time of findWithName is θ(1) as the
    //get method for arrays has a constant running time
	public String findWithName( String name )
	{
		return nameTable.get( name );
	}
    //Running Time of findWithNumber is θ(1) as the
    //get method for arrays has a constant running time
	public String findWithNumber( String number )
	{
		return numberTable.get( number );
	}
    //delete calls deleteWithNumber or deleteWithName
    //both of these methos have a Running Time of θ(1)
    //therefore delete should have a Running Time of θ(1)
	public boolean delete(String nameORnumber)
	{
		if( !isNumber( nameORnumber ) )
			return deleteWithName( nameORnumber );
		if( isNumber( nameORnumber ) )
			return deleteWithNumber( formatNumber( nameORnumber ) );
		return false;
	}
    //nameTable is a HashMap therefore when it
    //calls the remove method the Running Time
    //of deleteWithName is θ(1)
	public boolean deleteWithName( String name )
	{
		String numberToBeRemoved = nameTable.remove( name );
		if( numberToBeRemoved != null )
			if( numberTable.remove( numberToBeRemoved ) != null )
				return true;
		return false;
	}
    //numberTable is a HashMap therefore when it
    //calls the remove method the Running Time
    //of deleteWithNumber is θ(1)
	public boolean deleteWithNumber( String number )
	{
		String nameToBeRemoved = numberTable.remove( number );
		if( nameToBeRemoved != null )
			if( nameTable.remove( nameToBeRemoved ) != null )
				return true;
		return false;
	}
    // The HashMap nametable is calling a method
    //to determine its size. The Running Time of
    //size() is θ(1)
	public int size()
	{
		return nameTable.size();
	}
    //printAllContacts walks through the whole tale
    //to verify if a contact needs to be printed out
    // giving its Running Time θ(n)
	public void printAllContacts()
	{
		for (String key : nameTable.keySet())
		{
			System.out.print( "Name: " + key + "; Number: " );
			System.out.println( nameTable.get( key ) );
		}
	}

	private HashMap<String, String> nameTable;
	private HashMap<String, String> numberTable;
}
