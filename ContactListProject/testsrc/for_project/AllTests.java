package for_project;
//Abby Jeffers
//Cerina Bhavnani
//Emily Keller
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ContactListHashMapTest.class, SeparateChainingHashTableTest.class })
public class AllTests {

}
