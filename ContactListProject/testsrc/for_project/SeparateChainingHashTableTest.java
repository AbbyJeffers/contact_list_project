package for_project;
//Abby Jeffers
//Cerina Bhavnani
//Emily Keller
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class SeparateChainingHashTableTest {

	SeparateChainingHashTable sCHT;
	String name1, name2, name3, name4, name5, name6, number1, number2, number3, 
	number4, number5, number6;

	@Before
	public void setUp() {
		sCHT = new SeparateChainingHashTable(3);
		name1 = "NameONE";
		name2 = "NameTWO";
		name3 = "NameTHREE";
		name4 = "NameFOUR";
		name5 = "NameFIVE";
		name6 = "NameSIX";
		number1 = "01234567890";
		number2 = "012-3!45-67 890";
		number3 = "0987654321";
		number4 = "4";
		number5 = "5";
		number6 = "6";
	}
    //this test code was written following the guidlines set by authoritative websites online
    //wich is why they look so different from the provided sample code.
	@Test
	public void testFormatNumberShouldReturnAStringWithJustNumbers() {
		assertEquals(sCHT.formatNumber(number2),number1);
		assertNotEquals(sCHT.formatNumber(number3),number2);
	}
	@Test
	public void isNumberShouldReturnFalseWhenGivenAName() {
		assertFalse(sCHT.isNumber(name1));

	}

	@Test
	public void isNumberShouldReturnTrueWhenGivenANumber() {
		assertTrue(sCHT.isNumber(number2));

	}
	@Test
	public void insertShouldReturnTrue() {
		assertTrue(sCHT.insert(name1,number1));
	}
	@Test
	public void insertShouldReturnFalse() {
		assertFalse(sCHT.insert(number1,number1));
	}
	@Test
	public void findShouldReturnNullWhenGivenAWrongNameOrNumber() {
		assertNull(sCHT.find(number3));
		assertNull(sCHT.find(name3));
	}

	@Test
	public void findShouldReturnNotNullWhenGivenACorrectNameORNumber() {
		sCHT.insert(name2,number2);
		assertNotNull(sCHT.find(number1));
		assertNotNull(sCHT.find(name2));
	}

	@Test
	public void findShouldReturnTheCorrectNameORNumber() {
		sCHT.insert(name2,number3);
		assertEquals(sCHT.find(name2), number3);
		assertEquals(sCHT.find(number3), name2);
	}

	@Test
	public void findNameShouldReturnTheCorrectNumber() {
		sCHT.insert(name1,number1);
		assertEquals(sCHT.findName(name1), number1);
	}

	@Test
	public void findNumberShouldReturnTheCorrectName() {
		sCHT.insert(name1,number2);
		assertEquals(sCHT.findNumber(number1), name1);
	}
	@Test 
	public void deleteShouldReturnTrueWhenDeleted() {
		sCHT.insert(name2,number2);
		assertTrue(sCHT.delete(name2));
	}
	@Test 
	public void deleteShouldReturnFalseWhenGivenAWrongNumber() {
		assertFalse(sCHT.delete(number3));
	}
	@Test
	public void deleteWithNameShouldReturnTrueAndDeleteTheNameWhenGivenAName() {
		sCHT.insert(name2,number1);
		assertTrue(sCHT.deleteWithName(name2));
		assertNull(sCHT.find(name2));
	}
	@Test
	public void deleteWithNameShouldReturnFalseWhenGivenAWrongName() {
		sCHT.insert(name2,number1);
		assertFalse(sCHT.deleteWithName(name1));
		assertNotNull(sCHT.find(name2));//does not delete anything else.
	}
	@Test
	public void deleteWithNumberShouldReturnTrueAndDeleteTheNameWhenGivenANumber() {
		sCHT.insert(name2,number6);
		assertTrue(sCHT.deleteWithNumber(number6));
		assertNull(sCHT.find(name2));
	}
	@Test
	public void deleteWithNumberShouldReturnFalseWhenGivenAWrongNumber() {
		sCHT.insert(name2,number1);
		assertFalse(sCHT.deleteWithName(number2));
		assertNotNull(sCHT.find(name2));//does not delete anything else.
	}
	@Test
	public void theSizesShouldBeCorrect() {
		assertEquals(sCHT.size(),0);
		sCHT.insert(name2,number1);
		assertEquals(sCHT.size(),1);
		sCHT.delete(number1);
		assertEquals(sCHT.size(),0);
	}
	@Test 
	public void printAllContactsShouldPrintAllContacts() {
		sCHT.printAllContacts();
		sCHT.insert(name1,number1);
		sCHT.insert(name2,number2);
		sCHT.insert(name3,number3);
		sCHT.insert(name4,number4);
		sCHT.printAllContacts();
	}

	@Test
	public void rehashShouldCreateALargerTable() {
		sCHT.insert(name1,number1);
		sCHT.insert(name2,number2);
		sCHT.insert(name3,number3);
		sCHT.insert(name4,number4);
		sCHT.insert(name5,number5);
		sCHT.insert(name6,number6);
	}

	@Test
	public void insertShouldNotInsertDuplicates() {
		assertTrue(sCHT.insert(name1,number1));
		assertFalse(sCHT.insert(name1,number1));
	}
}
