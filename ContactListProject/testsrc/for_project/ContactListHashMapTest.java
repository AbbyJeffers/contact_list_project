package for_project;
//Abby Jeffers
//Cerina Bhavnani
//Emily Keller
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class ContactListHashMapTest {

	ContactListHashMap cLHM;
	
	String name1, name2, name3, name4, name5, name;
	String number1, number2, number3, number4, number5;
	
	@Before
	public void setUp() throws Exception{
		cLHM = new ContactListHashMap();
		name1 = "NameOne";
		name2 = "NameTwo";
		name3 = "N3";
		name4 = "N4";
		name5 = "N5";
		number1 = "12-3456-78  90";
		number2 = "1234567890";
		number3 = "1234 567  890";
		number4 = "12    34 56-78 90";
		number5 = "12";
	}
	//this test code was written following the guidlines set by authoritative websites online
    //wich is why they look so different from the provided sample code. 
	@Test
	public void formatNumberShouldReturnAStringWithJustNumbers() {
		assertEquals(cLHM.formatNumber(number1),number2);
		assertNotEquals(cLHM.formatNumber(number5),number2);
	}

	@Test
	public void isNumberShouldReturnFalseWhenGivenAName() {
		assertFalse(cLHM.isNumber(name1));
		
	}
	
	@Test
	public void isNumberShouldReturnTrueWhenGivenANumber() {
		assertTrue(cLHM.isNumber(number5));
		
	}
	
	@Test
	public void insertShouldNotInsertDuplicates() {
		assertTrue(cLHM.insert(name1,number1));
		assertFalse(cLHM.insert(name1,number1));
	}
	
	@Test
	public void findShouldReturnNullWhenGivenAWrongNameOrNumber() {
		assertNull(cLHM.find(number4));
		assertNull(cLHM.find(name4));
	}
	
	@Test
	public void findShouldReturnNotNullWhenGivenACorrectNameORNumber() {
		cLHM.insert(name2,number4);
		assertNotNull(cLHM.find(number4));
		assertNotNull(cLHM.find(name2));
	}
	
	@Test
	public void findWithNameShouldReturnTheCorrectNameORNumber() {
		cLHM.insert(name2,number4);
		assertEquals(cLHM.find(name2), number2);
		assertEquals(cLHM.find(number4), name2);
	}
	
	@Test
	public void findWithNameShouldReturnTheCorrectNumber() {
		cLHM.insert(name1,number4);
		 assertEquals(cLHM.findWithName(name1), number2);
	}
	
	@Test
	public void findWithNumberShouldReturnTheCorrectName() {
		cLHM.insert(name1,number2);
		 assertEquals(cLHM.findWithNumber(number2), name1);
	}
	
	@Test 
	public void deleteShouldReturnTrueWhenDeleted() {
		cLHM.insert(name2,number4);
		assertTrue(cLHM.delete(name2));
	}
	@Test 
	public void deleteShouldReturnFalseWhenGivenAWrongNumber() {
		assertFalse(cLHM.delete(name5));
	}
	@Test
	public void deleteWithNameShouldReturnTrueAndDeleteTheNameWhenGivenAName() {
		cLHM.insert(name2,number4);
		assertTrue(cLHM.deleteWithName(name2));
		assertNull(cLHM.find(name2));
	}
	@Test
	public void deleteWithNameShouldReturnFalseWhenGivenAWrongName() {
		cLHM.insert(name2,number4);
		assertFalse(cLHM.deleteWithName(name1));
		assertNotNull(cLHM.find(name2));//does not delete anything else.
	}
	@Test
	public void deleteWithNumberShouldReturnTrueAndDeleteTheNameWhenGivenANumber() {
		cLHM.insert(name2,number4);
		assertTrue(cLHM.deleteWithNumber(number2));
		assertNull(cLHM.find(name2));
	}
	@Test
	public void deleteWithNumberShouldReturnFalseWhenGivenAWrongNumber() {
		cLHM.insert(name2,number4);
		assertFalse(cLHM.deleteWithName(number2));
		assertNotNull(cLHM.find(name2));//does not delete anything else.
	}
	@Test
	public void theSizesShouldBeCorrect() {
		assertEquals(cLHM.size(),0);
		cLHM.insert(name2,number4);
		assertEquals(cLHM.size(),1);
	}
	@Test 
	public void printAllContactsShouldPrintAllContacts() {
		cLHM.printAllContacts();
		cLHM.insert(name2,number4);
		cLHM.insert(name1,number5);
		cLHM.printAllContacts();
	}
	
}
